# Opensearx configuration generator

## Usage

```bash
Usage: generate_opensearx_config [OPTIONS]

  Opensearx configuration generator.

  Args:     configuration_file: Opensearx YAML configuration file     output:
  Output file path to generated Opensearx configuration     verbose: level of
  verbosity     force: overwrite existing output file

Options:
  -c, --configuration-file PATH  YAML configuration file  [required]
  -o, --output PATH              YAML configuration file
  -v, --verbose
  --force
  --version                      Show the version and exit.
  --help                         Show this message and exit.
```

### Configuration

#### Public metadata only

```yaml
harvest_protected_metadata: true

csw:
  base_url: 'https://sextant.ifremer.fr/geonetwork/GHRSST/'
  endpoint: 'eng'
  page_size: 100

opensearch_engines:
  'https://opensearch.ifremer.fr/':
    name: 'OSI SAF Opensearch service'
    engine_class: 'IfremerOpensearchEngine'
    root_path: 'https://opensearch.ifremer.fr/granules.atom'
    timeout: 30.0
  'https://cmr.earthdata.nasa.gov/opensearch/':
    name: 'PODAAC Opensearch service'
    engine_class: 'JPLOpensearchEngine'
    root_path: 'https://cmr.earthdata.nasa.gov/opensearch/granules.atom'
    timeout: 30.0
```

#### Public and private metadata

Add a section `http_client` to use a CAS client.

```yaml
harvest_protected_metadata: true

csw:
  base_url: 'https://sextant.ifremer.fr/geonetwork/GHRSST/'
  endpoint: 'eng'
  page_size: 100

http_client:
  client_name: 'Cas'
  config:
    cas_host: 'auth.ifremer.fr'
    username: 'login'
    password: 'password'
    service_url: 'https://sextant.ifremer.fr/geonetwork/GHRSST/eng/catalog.search'

opensearch_engines:
  'https://opensearch.ifremer.fr/':
    name: 'OSI SAF Opensearch service'
    engine_class: 'IfremerOpensearchEngine'
    root_path: 'https://opensearch.ifremer.fr/granules.atom'
    timeout: 30.0
  'https://cmr.earthdata.nasa.gov/opensearch/':
    name: 'PODAAC Opensearch service'
    engine_class: 'JPLOpensearchEngine'
    root_path: 'https://cmr.earthdata.nasa.gov/opensearch/granules.atom'
    timeout: 30.0
```

### Execution

```bash
# display configuration to stdout
generate_opensearx_config -c generator.yaml
# write configuration in a file
generate_opensearx_config -c generator.yaml --output opensearx.yaml
# verbosity = DEBUG
generate_opensearx_config -c generator.yaml --output opensearx.yaml -vvv
```

Here is a sample output configuration :

```yaml
datasets:
- OSI-204

opensearch_engines:
- datasets_mapping:
    OSI-204: avhrr_sst_metop_a-osisaf-l2p-v1.0
  engine_class: IfremerOpensearchEngine
  name: OSI SAF Opensearch service
  root_path: https://opensearch.ifremer.fr/granules.atom
  timeout: 30.0
- datasets_mapping:
    OSI-204: C1664387028-PODAAC
  engine_class: JPLOpensearchEngine
  name: PODAAC Opensearch service
  root_path: https://cmr.earthdata.nasa.gov/opensearch/granules.atom
  timeout: 30.0
```

## Development

### Install dependencies

- installer poetry

```bash
pip install poetry poetry-dynamic-versioning
```

- installer les dépendances et le projet

```bash
poetry install -vv
```

### Pre-commit

- register pre-commit

```bash
pre-commit install
```

- Run the hooks manually

```bash
pre-commit run --all-files
```

### Check code quality

```bash
flake8 .
```

### Run unit tests

```bash
pytest --tb=line
```
