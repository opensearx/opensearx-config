"""CSW parser module."""
import logging
import math
from typing import Dict, Iterator, List, Optional

from lxml import etree
from requests import Response
from requests.exceptions import HTTPError

from opensearx_config.csw.model import CswConfig, CswDatasetLinks, CswOnLineResource
from opensearx_config.http.client.base import HTTPClient
from opensearx_config.http.helpers import add_url_params

_log = logging.getLogger(__name__)


class CswException(Exception):
    """CSW exception."""


def parse_response(response: Response) -> etree.ElementTree:
    """Parse response to XML.

    Args:
        response: request response

    Returns:
        Content formatted as XML

    Raises:
        CswException: if response status is invalid
    """
    try:
        response.raise_for_status()
        return etree.fromstring(response.content)
    except (HTTPError, etree.XMLSyntaxError) as error:
        raise CswException(error)


def get_number_of_results(
    csw_xml: etree.ElementTree,
    namespaces: Optional[Dict] = None,
) -> int:
    """Retrieve the number of records.

    Args:
        csw_xml: CSW content as XML
        namespaces: to parse XML document

    Returns:
        Number of records
    """
    if not namespaces:
        namespaces = csw_xml.nsmap
    search_results = csw_xml.find('csw:SearchResults', namespaces=namespaces)
    return int(search_results.attrib['numberOfRecordsMatched'])


def get_identifiers(
    csw_xml: etree.ElementTree,
    namespaces: Optional[Dict] = None,
) -> List[str]:
    """Retrieve the list of metadata uid from a CSW XML document.

    Args:
        csw_xml: CSW content as XML
        namespaces: to parse XML document

    Returns:
        List of metadata identifiers
    """
    if not namespaces:
        namespaces = csw_xml.nsmap
    records = csw_xml.findall('.//csw:SearchResults/csw:BriefRecord', namespaces=namespaces)
    return [record.find('dc:identifier', namespaces=namespaces).text for record in records]


def get_dataset_id(metadata_xml: etree.ElementTree, namespaces: Dict = None) -> Optional[str]:
    """Retrieve dataset id from xml.

    Args:
        metadata_xml: xml content
        namespaces: namespaces to decode xml

    Returns:
        dataset id if found
    """
    try:
        return metadata_xml.find(
            './mdb:identificationInfo/mri:MD_DataIdentification/mri:citation/cit:CI_Citation/'
            'cit:identifier/mcc:MD_Identifier/mcc:code/gco:CharacterString',
            namespaces=namespaces,
        ).text

    except AttributeError:
        return None


def get_online_resources(metadata_xml: etree.ElementTree, namespaces: Dict = None) -> Optional[List]:
    """Retrieve dataset id from xml.

    Args:
        metadata_xml: xml content
        namespaces: namespaces to decode xml

    Returns:
        list of online resources
    """
    try:
        return metadata_xml.xpath(
            './/mdb:distributionInfo/mrd:MD_Distribution/mrd:transferOptions/'
            'mrd:MD_DigitalTransferOptions/mrd:onLine/cit:CI_OnlineResource',
            namespaces=namespaces,
        )
    except AttributeError:
        return None


def get_opensearch_online_resource(link_xml: etree.ElementTree, namespaces: Dict = None) -> Optional[CswOnLineResource]:
    """Transform opensearch link to csw online resource.

    Args:
        link_xml: xml content
        namespaces: namespaces to decode xml

    Returns:
        Csw online resource instance
    """
    try:
        protocol = link_xml.find('./cit:protocol/gco:CharacterString', namespaces=namespaces).text
        if protocol == 'WWW:OPENSEARCH':
            return CswOnLineResource(
                protocol=protocol,
                linkage=link_xml.find('./cit:linkage/gco:CharacterString', namespaces=namespaces).text.rstrip('/'),
                name=link_xml.find('./cit:name/gco:CharacterString', namespaces=namespaces).text,
                description=link_xml.find('./cit:description/gco:CharacterString', namespaces=namespaces).text,
            )
    except AttributeError:
        return None


def dataset_links(
    metadata_xml: etree.ElementTree,
    namespaces: Optional[Dict] = None,
) -> Optional[CswDatasetLinks]:
    """Retrieve the number of records.

    Args:
        metadata_xml: Metadata content as XML
        namespaces: to parse XML document

    Returns:
        Dataset with it opensearch links
    """
    if not namespaces:
        namespaces = metadata_xml.nsmap

    # get product ID
    dataset_id = get_dataset_id(metadata_xml, namespaces)
    if not dataset_id:
        return None

    # get opensearch online resources
    opensearch_links = []
    for link_xml in get_online_resources(metadata_xml, namespaces):
        csw_online_resource = get_opensearch_online_resource(link_xml, namespaces)
        if csw_online_resource:
            opensearch_links.append(csw_online_resource)

    if not opensearch_links:
        return None
    return CswDatasetLinks(dataset_id=dataset_id, opensearch_links=opensearch_links)


class CswParser(object):
    """Csw parser class."""

    def __init__(self, http_client: HTTPClient, csw_config: CswConfig):
        """Initialize attributes.

        Args:
            http_client: http client instance
            csw_config: csw configuration instance
        """
        self._log = logging.getLogger(__name__)
        self.http_client = http_client
        self.csw_config = csw_config

    def connect(self):
        """Create session to geonetwork."""
        self.http_client.get(self.csw_config.catalog_url, with_st=True)

    def nb_metadata(self):
        """Retrieve the number of metadata.

        Returns:
            Number of metadata
        """
        # request CSW catalog
        url = add_url_params(self.csw_config.csw_url, {
            'version': '2.0.2',
            'service': 'CSW',
            'request': 'GetRecords',
            'resultType': 'hits',  # hits = just number of results
            'typeNames': 'csw:Record',
        })
        # execute request and parse number of results
        response = self.http_client.get(url, with_st=False)
        return get_number_of_results(
            csw_xml=parse_response(response),
            namespaces=self.csw_config.catalog_namespaces,
        )

    def identifiers(self, start: int = 1) -> List[str]:
        """Retrieve all metadata identifiers.

        Args:
            start: page number

        Returns:
            List of identifiers in the current page
        """
        # request CSW catalog
        url = add_url_params(self.csw_config.csw_url, {
            'version': '2.0.2',
            'service': 'CSW',
            'request': 'GetRecords',
            'resultType': 'results',
            'typeNames': 'csw:Record',
            'ElementSetName': 'brief',
            'startPosition': start,
            'maxRecords': self.csw_config.page_size,
        })
        return get_identifiers(
            csw_xml=parse_response(self.http_client.get(url, with_st=False)),
            namespaces=self.csw_config.catalog_namespaces,
        )

    def opensearch_links(self, metadata_uid: str) -> Optional[CswDatasetLinks]:
        """Retrieve opensearch links for a metadata.

        Args:
            metadata_uid: metadata identifier

        Returns:
            Dataset ID and list of opensearch links
        """
        url = self.csw_config.metadata_url(metadata_uid)
        response = self.http_client.get(url, with_st=False)
        return dataset_links(
            metadata_xml=parse_response(response),
            namespaces=self.csw_config.metadata_namespaces,
        )

    def datasets_opensearch_links(self) -> Dict[str, List[CswOnLineResource]]:
        """Retrieve all metadata with an opensearch link.

        Returns:
            List of <dataset id, List of opensearch links>
        """
        datasets: Dict[str, List[CswOnLineResource]] = {}
        # retrieve number of metadata to process
        nb_results = self.nb_metadata()
        self._log.info('Nb metadata to process : {0}'.format(nb_results))
        # calculate nb loops
        nb_loop = math.ceil(nb_results / self.csw_config.page_size)
        current_page = 0
        # for each page
        while current_page < nb_loop:
            self._log.debug('Loop {0}/{1}'.format(current_page + 1, nb_loop))
            for csw_dataset_links in self.process_page(current_page):
                if not csw_dataset_links:
                    continue
                if csw_dataset_links.dataset_id not in datasets:
                    datasets[csw_dataset_links.dataset_id] = []
                datasets[csw_dataset_links.dataset_id].extend(csw_dataset_links.opensearch_links)
            current_page = current_page + 1
        return datasets

    def process_page(self, current_page: int) -> Iterator[CswDatasetLinks]:
        """Retrieve opensearch links for a page.

        Args:
            current_page: current csw page

        Yields:
            dataset id and links of an identifier
        """
        # retrieve all identifiers
        identifiers = self.identifiers((current_page * self.csw_config.page_size) + 1)

        # retrieve all datasets with their links
        for identifier in identifiers:
            self._log.debug('Process identifier : {0}'.format(identifier))
            try:
                yield self.opensearch_links(identifier)
            except CswException:
                self._log.warning('Error while parsing metadata {0}'.format(identifier))
