"""CSW models module."""
from typing import Dict, List

from pydantic import BaseModel


class CswOnLineResource(BaseModel):
    """OnLineResource model."""

    protocol: str
    linkage: str
    name: str
    description: str


class CswDatasetLinks(BaseModel):
    """Dataset with his opensearch links model."""

    dataset_id: str
    opensearch_links: List[CswOnLineResource] = []


class CswConfig(BaseModel):
    """Cas parser configuration model."""

    base_url: str
    endpoint: str = 'eng'
    page_size: int = 100

    xpath_csw_record: str = './/csw:SearchResults/csw:BriefRecord'

    xpath_product_id: str = """
        ./mdb:identificationInfo/mri:MD_DataIdentification/mri:citation/
        cit:CI_Citation/cit:alternateTitle/gco:CharacterString
    """

    xpath_online_resource: str = """
        ./mdb:distributionInfo/mrd:MD_Distribution/mrd:transferOptions/
        mrd:MD_DigitalTransferOptions/mrd:onLine/cit:CI_OnlineResource
    """

    catalog_namespaces: Dict = {
        'csw': 'http://www.opengis.net/cat/csw/2.0.2',
        'dc': 'http://purl.org/dc/elements/1.1/',
    }

    metadata_namespaces: Dict = {
        'mdb': 'http://standards.iso.org/iso/19115/-3/mdb/2.0',
        'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'cat': 'http://standards.iso.org/iso/19115/-3/cat/1.0',
        'gfc': 'http://standards.iso.org/iso/19110/gfc/1.1',
        'cit': 'http://standards.iso.org/iso/19115/-3/cit/2.0',
        'gcx': 'http://standards.iso.org/iso/19115/-3/gcx/1.0',
        'gex': 'http://standards.iso.org/iso/19115/-3/gex/1.0',
        'lan': 'http://standards.iso.org/iso/19115/-3/lan/1.0',
        'srv': 'http://standards.iso.org/iso/19115/-3/srv/2.1',
        'mas': 'http://standards.iso.org/iso/19115/-3/mas/1.0',
        'mcc': 'http://standards.iso.org/iso/19115/-3/mcc/1.0',
        'mco': 'http://standards.iso.org/iso/19115/-3/mco/1.0',
        'mda': 'http://standards.iso.org/iso/19115/-3/mda/1.0',
        'mds': 'http://standards.iso.org/iso/19115/-3/mds/2.0',
        'mdt': 'http://standards.iso.org/iso/19115/-3/mdt/2.0',
        'mex': 'http://standards.iso.org/iso/19115/-3/mex/1.0',
        'mmi': 'http://standards.iso.org/iso/19115/-3/mmi/1.0',
        'mpc': 'http://standards.iso.org/iso/19115/-3/mpc/1.0',
        'mrc': 'http://standards.iso.org/iso/19115/-3/mrc/2.0',
        'mrd': 'http://standards.iso.org/iso/19115/-3/mrd/1.0',
        'mri': 'http://standards.iso.org/iso/19115/-3/mri/1.0',
        'mrl': 'http://standards.iso.org/iso/19115/-3/mrl/2.0',
        'mrs': 'http://standards.iso.org/iso/19115/-3/mrs/1.0',
        'msr': 'http://standards.iso.org/iso/19115/-3/msr/2.0',
        'mdq': 'http://standards.iso.org/iso/19157/-2/mdq/1.0',
        'mac': 'http://standards.iso.org/iso/19115/-3/mac/2.0',
        'gco': 'http://standards.iso.org/iso/19115/-3/gco/1.0',
        'gml': 'http://www.opengis.net/gml/3.2',
        'xlink': 'http://www.w3.org/1999/xlink',
    }

    @property
    def endpoint_url(self) -> str:
        """Retrieve base endpoint url.

        Returns:
            Url to base endpoint
        """
        return '{0}{1}'.format(self.base_url, self.endpoint)

    @property
    def catalog_url(self) -> str:
        """Retrieve csw catalog endpoint url.

        Returns:
            Url to catalog endpoint
        """
        return '{0}/catalog.search'.format(self.endpoint_url)

    @property
    def csw_url(self) -> str:
        """Retrieve csw endpoint url.

        Returns:
            Url to csw endpoint
        """
        return '{0}/csw'.format(self.endpoint_url)

    def metadata_url(self, metadata_uid: str) -> str:
        """Retrieve metadata xml content url.

        Args:
            metadata_uid: metadata identifier

        Returns:
            url to metadata xml content
        """
        return '{0}/api/records/{1}/formatters/xml'.format(self.base_url.rstrip('/'), metadata_uid)
