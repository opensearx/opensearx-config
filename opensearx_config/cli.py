"""Console clients module."""
import logging
import sys
from pathlib import Path

import click

from opensearx_config.core.helpers import get_log_level
from opensearx_config.generator import generate_configuration, serialize, write_configuration
from opensearx_config.model import Format, GeneratorConfig


class EnumType(click.Choice):
    """Manage choice from an enum."""

    def __init__(self, enum, case_sensitive=False):
        """Initialize class.

        Args:
            enum:  enum type
            case_sensitive: case sensitive
        """
        self.__enum = enum
        super().__init__(choices=[item.value for item in enum], case_sensitive=case_sensitive)

    def convert(self, value, param, ctx):
        """Convert value to enum.

        Args:
            value: value
            param: click param
            ctx: click contexte

        Returns:
            Enumeration item
        """
        converted_str = super().convert(value, param, ctx)
        return self.__enum(converted_str)


@click.command()
@click.option('-c', '--configuration-file', help='YAML configuration file', type=click.Path(), required=True)
@click.option('-o', '--output', help='YAML configuration file', type=click.Path())
@click.option('-v', '--verbose', count=True)
@click.option('--force', is_flag=True)
@click.option(
    '--format',
    'output_format',
    help='output format ({0})'.format(','.join([ef.value for ef in Format])),
    type=EnumType(Format),
    default=Format.JSON.value,
)
@click.version_option()
def generate_configuration_cli(
    configuration_file: Path,
    output_format: Format,
    output: Path,
    verbose: int = 0,
    force: bool = False,
) -> None:
    """Opensearx configuration generator.

    Args:
        configuration_file: Opensearx YAML configuration file
        output_format: output format
        output: Output file path to generated Opensearx configuration
        verbose: level of verbosity
        force: overwrite existing output file
    """
    # init logging
    logging.basicConfig(level=get_log_level(verbose))

    # generate opensearx configuration
    opensearx_config = generate_configuration(GeneratorConfig.from_yaml_file(configuration_file))

    # serialize configuration
    serialized_config = serialize(opensearx_config, output_format)

    # output configuration
    if not output:
        click.echo(serialized_config)
        sys.exit(0)

    try:
        write_configuration(serialized_config, output, force)
    except FileExistsError:
        click.echo(
            click.style('File {0} already exists ! Use `--force` option to override it.'.format(output), fg='red'),
            color=True,
        )


if __name__ == '__main__':
    generate_configuration_cli('../application.yaml')
