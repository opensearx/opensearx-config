"""Configuration generator service module."""
import collections
import json
import logging
from pathlib import Path
from typing import Dict, List, OrderedDict

import yaml

from opensearx_config.core.model import OpensearchEngine, OpensearxConfig
from opensearx_config.csw.model import CswOnLineResource
from opensearx_config.csw.parser import CswParser
from opensearx_config.http.client.factory import create_http_client
from opensearx_config.model import Format, GeneratorConfig

_log = logging.getLogger('opensearx.config')


def classify_datasets_by_links(
    datasets: Dict[str, List[CswOnLineResource]],
) -> OrderedDict[str, Dict[str, str]]:  # noqa: WPS221
    """Classify datasets by opensearch engine.

    Args:
        datasets: datasets classified by dataset ID

    Returns:
        Classified datasets
    """
    engines_by_links: OrderedDict[str, Dict[str, str]] = collections.OrderedDict()
    for dataset_id in sorted(datasets.keys()):
        for link in datasets[dataset_id]:
            if link.linkage not in engines_by_links:
                engines_by_links[link.linkage] = {}
            engines_by_links[link.linkage][dataset_id] = link.name
    return engines_by_links


def get_opensearx_configuration(
    engines: Dict[str, OpensearchEngine],
    datasets: Dict[str, List[CswOnLineResource]],
) -> OpensearxConfig:
    """Generate opensearx configuration.

    Args:
        engines: opensearch engines configuration
        datasets: datasets classified by dataset ID

    Returns:
        Opensearx configuration instance
    """
    # build opensearch engines configuration
    opensearch_engines = {}
    for url, dataset_mappings in classify_datasets_by_links(datasets).items():
        for dataset_id, online_resource in dataset_mappings.items():
            if url not in opensearch_engines:
                opensearch_engines[url] = OpensearchEngine(**engines[url].dict())
            opensearch_engines[url].engine_config.datasets_mapping[dataset_id] = online_resource

    # build and return the opensearx configuration
    return OpensearxConfig(
        datasets=sorted(datasets.keys()),
        opensearch_engines=list(opensearch_engines.values()),
    )


def generate_configuration(config: GeneratorConfig) -> OpensearxConfig:
    """Generate opensearx configuration service.

    Args:
        config: generator configuration service

    Returns:
        Opensearx configuration instance
    """
    _log.info('Create HTTP Client')
    http_client = create_http_client(
        module_name=config.http_client.module_name,
        client_name=config.http_client.client_name,
        **config.http_client.config,
    )

    csw_service = CswParser(http_client=http_client, csw_config=config.csw)

    # connect to geonetwork if needed
    if config.harvest_protected_metadata:
        _log.info('Connect to geonetwork')
        csw_service.connect()

    # harvest opensearch online resources
    _log.info('Harvest opensearch online resources')
    datasets = csw_service.datasets_opensearch_links()
    _log.info('- Nb datasets : {0}'.format(len(datasets)))

    # generate configuration
    _log.info('Generate opensearx configuration')
    return get_opensearx_configuration(
        engines=config.opensearch_engines,
        datasets=datasets,
    )


def serialize(opensearch_config: OpensearxConfig, output_format: Format = Format.YAML) -> str:
    """Serialize configuration.

    Args:
        opensearch_config: Opensearch configuration
        output_format: serialization format

    Returns:
        configuration serialized
    """
    config_as_dict = opensearch_config.dict()
    if output_format == Format.YAML:
        return yaml.dump(config_as_dict)
    if output_format == Format.JSON:
        return json.dumps(config_as_dict, indent=4)
    return str(config_as_dict)


def write_configuration(
    opensearch_config: str,
    output: Path,
    force: bool = False,
) -> None:
    """Write opensearx configuration.

    Args:
        opensearch_config: opensearx configuration instance
        output: path to output file
        force: if true, overwrite existing output file
    """
    _log.info('Write configuration to {0}'.format(output))
    with open(output, 'w' if force else 'x') as fd:
        fd.write(opensearch_config)
