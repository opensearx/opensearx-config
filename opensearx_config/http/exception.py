"""HTTP exceptions."""
from builtins import Exception


class HttpException(Exception):
    """Generic HTTP exception."""
