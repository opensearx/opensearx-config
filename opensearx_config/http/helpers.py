"""HTTP utilities module."""
from typing import Dict, List
from urllib import parse as url_parse

from pydantic import json


def add_url_params(url: str, query_params: dict) -> str:
    """Add GET params to provided URL being aware of existing.

    Args:
        url: string of target URL
        query_params: dict containing requested params to be added

    Returns:
        str: string with updated URL

    Examples:
        >> url = 'http://stackoverflow.com/test?answers=true'
        >> new_params = {'answers': False, 'data': ['some','values']}
        >> add_url_params(url, new_paramAs)
        'http://stackoverflow.com/test?data=some&data=values&answers=false'
    """
    # Extracting url info
    parsed_url = url_parse.urlparse(url_parse.unquote(url))
    # Converting URL arguments to dict
    parsed_get_args = dict(url_parse.parse_qsl(parsed_url.query))
    # Merging URL arguments dict with new params
    parsed_get_args.update(query_params)
    # Bool and Dict values should be converted to json-friendly values
    # you may throw this part away if you don't like it :)
    parsed_get_args.update({
        key: json.dumps(value)
        for key, value in parsed_get_args.items()
        if isinstance(value, (bool, dict))  # noqa: WPS221
    })
    # Creating new parsed result object based on provided with new
    # URL arguments. Same thing happens inside of urlparse.
    return url_parse.ParseResult(
        scheme=parsed_url.scheme,
        netloc=parsed_url.netloc,
        path=parsed_url.path,
        params=parsed_url.params,
        query=url_parse.urlencode(parsed_get_args, doseq=True),
        fragment=parsed_url.fragment,
    ).geturl()


def filter_get_post_kwargs(kwargs: Dict) -> Dict:
    """Keep only allowed get/post arguments.

    Args:
        kwargs: kwargs dictionary

    Returns:
        Dictionary with only allowed keys
    """
    allowed_kwargs: List = [
        'params',
        'data',
        'headers',
        'cookies',
        'files',
        'auth',
        'timeout',
        'allow_redirects',
        'proxies',
        'hooks',
        'stream',
        'verify',
        'cert',
        'json',
    ]
    return {key: kwargs[key] for key in kwargs.keys() if key in allowed_kwargs}
