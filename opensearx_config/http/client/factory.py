"""HTTP client factory module."""
import importlib
from typing import Optional

from opensearx_config.http.client.base import HTTPClient


def create_http_client(
    module_name: Optional[str] = None,
    client_name: Optional[str] = None,
    **config,
) -> HTTPClient:
    """HTTP Client factory.

    Args:
        module_name: python module name
        client_name: http client base name (ex: Base for BaseHTTPClient)
        config: http client configuration

    Returns:
        HTTP client instance
    """
    if not module_name:
        module_name = 'opensearx_config.http.client'
    if not client_name:
        client_name = 'Base'

    module = importlib.import_module('{0}.{1}'.format(module_name, client_name.lower()))
    client = getattr(module, '{0}HTTPClient'.format(client_name))
    return client(**config)
