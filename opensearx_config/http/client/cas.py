"""CAS HTTP client module."""
import re
from http.client import HTTPException
from typing import Dict, Optional
from urllib import parse as url_parse

import requests
from pydantic import BaseModel, SecretStr

from opensearx_config.http.client.base import BaseHTTPClient
from opensearx_config.http.helpers import add_url_params


class CasException(HTTPException):
    """Cas authentication exception."""


class CASConfig(BaseModel):
    """CAS client configuration."""

    cas_host: str
    cas_rest_endpoint: str = '/v1/tickets/'
    username: str
    password: SecretStr
    service_url: str


class CasHTTPClient(BaseHTTPClient):  # noqa: WPS214
    """CAS SSO client.

    Allow connecting to CAS and send GET or POST http requests.
    A session is used to store headers (cookies, ...).
    """

    # ticket granting ticket
    _tgt: Optional[str]
    # service ticket
    _st: Optional[str]
    # is authenticated to CAS
    _is_authenticated: bool

    def __init__(self, **config):
        """Initialize CasHTTPClient instance.

        Args:
            config: Cas configuration
        """
        super().__init__()
        self.config = CASConfig(**config)
        self._tgt = None
        self._st = None
        self._is_authenticated = False

    @property
    def ticket_granting_ticket(self) -> str:
        """Retrieve ticket granting ticket.

        Returns:
            Ticket granting ticket
        """
        return self._tgt

    @property
    def service_ticket(self) -> str:
        """Retrieve to service ticket.

        Returns:
            service ticket
        """
        return self._st

    def session(self) -> requests.Session:
        """Initialize HTTP session with a CAS connection.

        Returns:
            Session: http session
        """
        session = super().session()
        if not self._is_authenticated:
            self._log.debug('Retrieve CAS TGT')
            self._tgt = self._retrieve_tgt(session)
            self._log.debug('- {0}'.format(self._tgt))

            self._log.debug('Retrieve CAS ST')
            self._st = self._retrieve_st(session)
            self._log.debug('- {0}'.format(self._st))

            self._is_authenticated = True
        return session

    def close_session(self):
        """Logout from CAS and close HTTP session."""
        session = self.session()
        if self._is_authenticated:
            self._log.debug('Logout from CAS')
            session.get('https://{0}/logout'.format(self.config.cas_host))
            self._is_authenticated = False
        super().close_session()

    def get(self, url: str, with_st: bool = True, **kwargs) -> requests.Response:
        """Send an HTTP GET request.

        Connect first to CAS server if not authenticated.

        Args:
            url: url to service or file
            with_st: add service ticket to url (ticket=<service_ticket>) if True
            kwargs: request options (@see request.session.get)

        Returns:
            HTTP response
        """
        self.session()
        url = url if with_st is False else add_url_params(url, {'ticket': self._st})
        return super().get(url=url, **kwargs)

    def post(
        self,
        url: str,
        data: Optional[Dict] = None,
        xml_data: Optional[str] = None,
        json_data: Optional[str] = None,
        with_st: bool = True,
        **kwargs,
    ) -> requests.Response:
        """Send an HTTP GET request.

        Connect first to CAS server if not authenticated.

        Args:
            url: url to service or file
            data: request parameters
            xml_data: XML data (will set headers={'Content-Type': 'application/xml'} )
            json_data: JSON data (will set headers={'Content-Type': 'application/json'} )
            with_st: add service ticket to data (ticket=<service_ticket>) if True
            kwargs: request options (@see request.session.post)

        Returns:
            HTTP response
        """
        self.session()
        if with_st:
            if data is None:
                data = {}
            data['ticket'] = self._st

        return super().post(url=url, data=data, xml_data=xml_data, json_data=json_data, **kwargs)

    def _retrieve_tgt(self, session: requests.Session) -> str:
        """Retrieve ticket granting ticket.

        Args:
            session: http session

        Returns:
            str: ticket granting ticket

        Raises:
            CasException: if login failed
        """
        url = 'https://{0}{1}'.format(self.config.cas_host, self.config.cas_rest_endpoint)
        credentials = url_parse.urlencode({
            'username': self.config.username,
            'password': str(self.config.password.get_secret_value()),
        })
        response = session.post(
            url=url,
            data=credentials,
            headers={
                'Content-type': 'application/x-www-form-urlencoded',
                'Accept': 'text/plain',
                'User-Agent': 'python',
            },
        )
        try:
            location = response.headers['location']
        except KeyError:
            raise CasException('Authentication failed for {0} !'.format(url))

        return location[location.rfind('/') + 1:]

    def _retrieve_st(self, session: requests.Session) -> str:
        """Retrieve service ticket.

        Args:
            session: http session

        Returns:
            str: service ticket

        Raises:
            CasException: if ST generation failed
        """
        url = 'https://{0}{1}{2}'.format(self.config.cas_host, self.config.cas_rest_endpoint, self._tgt)
        response = session.post(
            url=url,
            data=url_parse.urlencode({'service': self.config.service_url}),
            headers={
                'Content-type': 'application/x-www-form-urlencoded',
                'Accept': 'text/plain',
                'User-Agent': 'python',
            })

        st = response.content.decode('utf-8')
        if re.search(r'ST-\d+-\w+-.*', st):
            return st

        raise CasException('Bad service ticket')
