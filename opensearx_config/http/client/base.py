"""Base HTTP client module."""
import logging
from typing import Dict, Optional

import requests

from opensearx_config.http.helpers import filter_get_post_kwargs


class HTTPClientMeta(type):
    """HTTP client meta class to define interface."""

    def __instancecheck__(cls, instance):
        """Check instance hook.

        Args:
            instance: class instance

        Returns:
            True if instance is compliant with HTTPClient interface
        """
        return cls.__subclasscheck__(type(instance))

    def __subclasscheck__(cls, subclass):
        """Check subclass is compliant with HTTPClient interface.

        Args:
            subclass: class instance

        Returns:
            True if instance is compliant with HTTPClient interface
        """
        check_session = (
            hasattr(subclass, 'session') and callable(subclass.session) and
            hasattr(subclass, 'close_session') and callable(subclass.close_session)
        )
        check_get_post = (
            hasattr(subclass, 'get') and callable(subclass.get) and
            hasattr(subclass, 'post') and callable(subclass.post)
        )
        return check_session and check_get_post


class HTTPClient(object, metaclass=HTTPClientMeta):
    """This interface is used for concrete classes to inherit from.

    There is no need to define the HTTPClient methods as any class
    as they are implicitly made available via .__subclasscheck__().
    """


class BaseHTTPClient(object):
    """Base HTT client class (without authentication)."""

    # logging manegement
    _log: logging.Logger
    # session management
    __session: requests.Session
    _is_session_initialized: bool

    def __init__(self):
        """Initialize BaseHTTPClient instance."""
        self._log = logging.getLogger(__name__)
        self._is_session_initialized = False

    def session(self) -> requests.Session:
        """Initialize HTTP session.

        Returns:
            Session: http session
        """
        if not self._is_session_initialized:
            self._log.debug('Create HTTP session')
            self.__session = requests.session()
            self._is_session_initialized = True
        return self.__session

    def close_session(self):
        """Close HTTP session."""
        if self._is_session_initialized:
            self._log.debug('Close HTTP session')
            self.__session.close()
            self._is_session_initialized = False

    def __enter__(self):
        """Set up the context manager.

        Returns:
            HTTP Client instance
        """
        return self

    def __exit__(self, exception_type, exception_value, exception_traceback):
        """Teardown context manager.

        Args:
            exception_type: indicates class of exception.
            exception_value: indicates type of exception (ex: divide_by_zero)
            exception_traceback: traceback is a report which has all of the information needed to solve the exception.
        """
        self.close_session()

    def get(self, url: str, **kwargs) -> requests.Response:
        """Send an HTTP GET request.

        Args:
            url: url to service or file
            kwargs: request options (@see request.session.get)

        Returns:
            HTTP response
        """
        session = self.session()
        return session.get(url=url, **filter_get_post_kwargs(kwargs))

    def post(
        self,
        url: str,
        data: Optional[Dict] = None,
        xml_data: Optional[str] = None,
        json_data: Optional[str] = None,
        **kwargs,
    ) -> requests.Response:
        """Send an HTTP POST request.

        Args:
            url: url to service or file
            data: request parameters
            xml_data: XML data (will set headers={'Content-Type': 'application/xml'} )
            json_data: JSON data (will set headers={'Content-Type': 'application/json'} )
            kwargs: request options (@see request.session.post)

        Returns:
            HTTP response
        """
        headers = {}
        if 'headers' in kwargs:
            headers = kwargs.pop('headers')

        if xml_data is not None:
            post_data = xml_data
            headers['Content-Type'] = 'application/xml'
        elif json_data is not None:
            post_data = json_data
            headers['Content-Type'] = 'application/json'
        else:
            if data is None:
                data = {}
            post_data = data

        return self.session().post(url, data=post_data, headers=headers, **filter_get_post_kwargs(kwargs))
