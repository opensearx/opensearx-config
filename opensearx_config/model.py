"""Model modules."""
from __future__ import annotations

from enum import Enum, unique
from typing import Dict

from opensearx_config.core.model import OpensearchEngine, OpensearxBaseModel, OpensearxBaseSettings
from opensearx_config.csw.model import CswConfig


class HTTPClientConfig(OpensearxBaseModel):
    """HTTP client configuration model."""

    module_name: str = 'opensearx_config.http.client'
    client_name: str = 'Base'
    config: Dict = {}


class GeneratorConfig(OpensearxBaseSettings):
    """Opensearx configuration generator settings."""

    harvest_protected_metadata: bool = True
    http_client: HTTPClientConfig = HTTPClientConfig()
    csw: CswConfig
    opensearch_engines: Dict[str, OpensearchEngine]


@unique
class Format(Enum):
    """Output format enumeration."""

    JSON = 'json'
    YAML = 'yaml'
    DICT = 'dict'
