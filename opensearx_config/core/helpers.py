"""Opensearx utitilies module."""
import logging
from typing import Dict, Optional

import yaml
from pydantic import BaseModel


def deep_merge(dict1: Dict, dict2: Dict) -> Dict:
    """Merge two dicts.

    If keys are conflicting, dict2 is preferred.

    Args:
        dict1: first dictionary
        dict2: second dictionary

    Returns:
        New dictionary
    """
    effective_dict = {}
    for key in dict1.keys() | dict2.keys():
        value1 = dict1.get(key)
        value2 = dict2.get(key)
        if isinstance(value1, dict) and isinstance(value2, dict):
            value = deep_merge(value1, value2)
        else:
            value = value2 or value1
        effective_dict[key] = value
    return effective_dict


def model_to_yaml(model: BaseModel) -> str:
    """Serialize Pydantic model to YAML content.

    Args:
        model: Pydantic model

    Returns:
        Yaml content
    """
    return yaml.dump(model.dict())


def get_log_level(verbosity: int = 0) -> Optional[int]:
    """Retrieve log level from int.

    Args:
        verbosity: int from 0 to 2

    Returns:
        Logging level
    """
    if not verbosity:
        verbosity = 0
    if verbosity > 3:
        verbosity = 3
    return {
        0: logging.ERROR,
        1: logging.WARNING,
        2: logging.INFO,
        3: logging.DEBUG,
    }[verbosity]
