"""Opensearx model/setting module."""
from __future__ import annotations

from pathlib import Path
from typing import Dict, List

import yaml
from pydantic import BaseModel, BaseSettings

from opensearx_config.core.helpers import deep_merge


class OpensearxBaseModel(BaseModel):
    """Base Opensearx-config  model."""

    class Config:   # noqa: WPS431, WPS431, WPS202, WPS306
        """Pydantic configuration."""

        validate_all = True
        validate_assignment = True


class OpensearxBaseSettings(BaseSettings):
    """Opensearx base settings class."""

    @classmethod
    def from_yaml_file(cls, file: Path):
        """Load settings from an YAML file.

        Args:
            file: path to YAML file

        Returns:
            Pydantic settings instance
        """
        with open(file) as fd:
            return cls(**yaml.safe_load(fd))

    def to_yaml(self) -> str:
        """Dump model to YAML.

        Returns:
            Settings exported as Yaml content
        """
        return yaml.dump(self.dict())

    def merge(self, partial: Dict) -> OpensearxBaseSettings:
        """Merge from a partial dictionary.

        Args:
            partial: dictionary to merge

        Returns:
            New instance of OpensearxBaseSettings
        """
        if partial is None:
            return self
        merge_dicts = deep_merge(self.dict(), partial)
        return self.parse_obj(merge_dicts)


class OpensearchEngineConfig(OpensearxBaseModel):
    """Opensearch engine configuration model."""

    name: str
    source_name: str
    documentation_url: str = None
    endpoint: str
    timeout: float = 30.0
    datasets_mapping: Dict = {}
    filter_protocols: Dict[str, Dict[str, List]] = None


class OpensearchEngine(OpensearxBaseModel):
    """OpenSearchEngine model."""

    engine_class: str
    engine_config: OpensearchEngineConfig


class OpensearxConfig(OpensearxBaseSettings):
    """Opensearx configuration settings."""

    datasets: List[str]
    opensearch_engines: List[OpensearchEngine]
