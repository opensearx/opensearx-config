"""Opensearx exceptions module."""


class OpensearxException(Exception):
    """Generic Opensearx Exception."""
