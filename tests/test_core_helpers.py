"""Test core helpers module."""
import logging
from typing import Dict

import pytest
import yaml

from opensearx_config.core.helpers import deep_merge, get_log_level, model_to_yaml
from opensearx_config.model import HTTPClientConfig


@pytest.mark.parametrize('verbosity, expected', [
    (0, logging.ERROR),
    (None, logging.ERROR),
    (1, logging.WARNING),
    (2, logging.INFO),
    (3, logging.DEBUG),
    (10, logging.DEBUG),
])
def test_get_log_level(verbosity: int, expected: int):
    """Test get log level function.

    Args:
        verbosity: input verbosity
        expected: logging level
    """
    assert get_log_level(verbosity) == expected


def test_model_to_yaml():
    """Test exporting model to yaml."""
    config = HTTPClientConfig()
    yaml_content = model_to_yaml(config)
    generated_config = HTTPClientConfig.parse_obj(yaml.safe_load(yaml_content))
    assert generated_config == config


@pytest.mark.parametrize('dict1, dict2, expected_dict', [
    ({'key1': 'value1'}, {'key2': 'value2'}, {'key1': 'value1', 'key2': 'value2'}),
    ({'key1': 'value1'}, {'key1': 'value2'}, {'key1': 'value2'}),
    ({'key1': {'subkey1': 'value1'}}, {'key1': 'value2'}, {'key1': 'value2'}),
    (
        {'key1': {'subkey1': 'value1'}},
        {'key1': {'subkey2': 'value2'}},
        {'key1': {'subkey1': 'value1', 'subkey2': 'value2'}},
    ),
])
def test_deep_merge(dict1: Dict, dict2: Dict, expected_dict: Dict):
    """Test deep_merge function.

    Args:
        dict1: main dictionary
        dict2: partial dictionary
        expected_dict: expect dictionary
    """
    assert deep_merge(dict1, dict2) == expected_dict
