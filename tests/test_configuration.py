"""Test generator configuration."""
from opensearx_config.model import GeneratorConfig


def test_generator_configuration(app_config: GeneratorConfig) -> None:
    """Test generator configuration serialization.

    Args:
        app_config: application configuration
    """
    assert app_config.harvest_protected_metadata is True
    assert app_config.http_client.client_name == 'Base'
