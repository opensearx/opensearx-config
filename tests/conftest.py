"""Pytest common fixture."""
import glob
import os
from pathlib import Path

from pytest import fixture

from opensearx_config.core.model import OpensearchEngine, OpensearchEngineConfig, OpensearxConfig
from opensearx_config.csw.model import CswConfig
from opensearx_config.csw.parser import CswParser
from opensearx_config.http.client.base import HTTPClient
from opensearx_config.http.client.factory import create_http_client
from opensearx_config.model import GeneratorConfig
from tests.helpers import read_file

DEFAULT_TIMEOUT: float = 30.0


@fixture(scope='session')
def test_dir() -> Path:
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return Path(__file__).parent


@fixture(scope='session')
def resources_dir(test_dir) -> Path:
    """Path to resource dir fixture.

    Args:
        test_dir: test directory fixture

    Returns:
        Path to resource dir
    """
    return test_dir / 'resources'


@fixture(scope='session')
def app_config_file(test_dir) -> Path:
    """Path to configuration file fixture.

    Args:
        test_dir: test directory fixture

    Returns:
        Path to configuration file
    """
    return test_dir / 'application.yaml'


@fixture(scope='session')
def app_config(app_config_file) -> GeneratorConfig:
    """Application configuration fixture.

    Args:
        app_config_file: path to configuration file fixture

    Returns:
        Path to configuration file
    """
    config = GeneratorConfig.from_yaml_file(app_config_file)
    if config.http_client.client_name.upper() == 'CAS':
        if 'CAS_USER' in os.environ:
            config.http_client.config['username'] = os.environ.get('CAS_USER')
        if 'CAS_PWD' in os.environ:
            config.http_client.config['password'] = os.environ.get('CAS_PWD')
    return config


@fixture(scope='session')
def csw_config(app_config: GeneratorConfig) -> CswConfig:
    """Csw configuration part fixture.

    Args:
        app_config: generator configuration

    Returns:
        Csw configuration part

    """
    return app_config.csw


@fixture
def http_client(app_config) -> HTTPClient:
    """Http client fixture.

    Args:
        app_config: generator configuration

    Yields:
        Http client instance
    """
    http_client: HTTPClient = create_http_client(
        module_name=app_config.http_client.module_name,
        client_name=app_config.http_client.client_name,
        **app_config.http_client.config,
    )
    yield http_client
    http_client.close_session()


@fixture()
def csw_parser(http_client: HTTPClient, csw_config: CswConfig) -> CswParser:
    """Csw parser fixture.

    Args:
        http_client: http client fixture
        csw_config: csw configuration fixture

    Returns:
        Csw parser instance
    """
    return CswParser(http_client=http_client, csw_config=csw_config)


@fixture(autouse=True)
def csw_mock(resources_dir, requests_mock, csw_config):
    """Csw mock fixture.

    Emulate access to sextant.

    Args:
        resources_dir: path to resources dir fixture
        requests_mock: requests mock fixture
        csw_config: csw configuration fixture

    Yields:
        requests mock
    """
    csw_test_dir = resources_dir / 'csw'

    # mock login to geonetwork
    requests_mock.get('{0}/catalog.search'.format(csw_config.endpoint_url))

    # mock csw-summary
    requests_mock.get(
        '{0}?version=2.0.2&service=CSW&request=GetRecords&resultType'
        '=hits&typeNames=csw%3ARecord'.format(csw_config.csw_url),
        text=read_file(csw_test_dir / 'csw-summary.xml'),
    )

    # mock csw-brief
    requests_mock.get(
        '{0}?version=2.0.2&service=CSW&request=GetRecords&resultType=results&typeNames=csw%3ARecord'
        '&ElementSetName=brief&startPosition=1&maxRecords=100'.format(csw_config.csw_url),
        text=read_file(csw_test_dir / 'csw-brief.xml'),
    )

    # mock metadata
    for metadata_file in glob.iglob(str(csw_test_dir / 'metadata' / '*.xml')):
        metadat_file_as_path = Path(metadata_file)
        requests_mock.get(
            csw_config.metadata_url(metadat_file_as_path.stem),
            text=read_file(metadat_file_as_path),
        )

    # mock
    yield requests_mock


@fixture
def output_configuration_path(tmp_path: Path) -> Path:
    """Output configuration path fixture.

    Args:
        tmp_path: temp directory as Path fixture

    Returns:
        Output file path
    """
    return tmp_path / 'opensearx.yml'


@fixture(scope='session')
def expected_configuration() -> OpensearxConfig:
    """Build expected opensearx configuration fixture.

    Returns:
        Opensearx configuration instance
    """
    return OpensearxConfig(
        datasets=['AVHRR19_L-NAVO-L2P-v1.0'],
        opensearch_engines=[
            OpensearchEngine(
                engine_class='podaac',
                engine_config=OpensearchEngineConfig(
                    name='PO.DAAC Opensearch service',
                    source_name='PODAAC',
                    documentation_url='https://cmr.earthdata.nasa.gov/opensearch',
                    endpoint='https://cmr.earthdata.nasa.gov/opensearch/granules.atom',
                    datasets_mapping={
                        'AVHRR19_L-NAVO-L2P-v1.0': 'C1655117103-PODAAC',
                    },
                    filter_protocols={
                        'HTTPS': [
                            'The HTTP location for the granule.',
                            'The base directory location for the granule.',
                        ],
                    },
                ),
            ),
        ],
    )
