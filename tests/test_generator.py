"""Test configuration generator service."""
from opensearx_config.core.model import OpensearxConfig
from opensearx_config.generator import generate_configuration, write_configuration
from opensearx_config.model import GeneratorConfig


def test_generate_configuration(app_config: GeneratorConfig, expected_configuration: OpensearxConfig) -> None:
    """Test configuration generation.

    Args:
        app_config: application configuration
        expected_configuration: expected generated configuration
    """
    output_config = generate_configuration(app_config)
    print(output_config.json())
    assert output_config == expected_configuration


def test_write_configuration(app_config, output_configuration_path, expected_configuration) -> None:
    """Test write generated configuration.

    Args:
        app_config: application configuration
        output_configuration_path: output file path
        expected_configuration: expected generated configuration
    """
    opensearx_config = generate_configuration(app_config)
    write_configuration(
        opensearch_config=opensearx_config.to_yaml(),
        output=output_configuration_path,
    )
    generated_config = OpensearxConfig.from_yaml_file(output_configuration_path)
    assert opensearx_config == generated_config == expected_configuration
