"""Test CSW parser."""
from typing import List

import lxml
import pytest
from requests import Response

from opensearx_config.csw.model import CswDatasetLinks, CswOnLineResource
from opensearx_config.csw.parser import CswException, parse_response


@pytest.fixture
def expected_osi204_links() -> List[CswOnLineResource]:
    """Csw online resources for OSI-204 dataset fixture.

    Returns:
        List of online resources.
    """
    return [
        CswOnLineResource(
            protocol='WWW:OPENSEARCH',
            linkage='https://opensearch.ifremer.fr',
            name='avhrr_sst_metop_a-osisaf-l2p-v1.0',
            description='OSI SAF Opensearch service',
        ),
        CswOnLineResource(
            protocol='WWW:OPENSEARCH',
            linkage='https://cmr.earthdata.nasa.gov/opensearch',
            name='C1664387028-PODAAC',
            description='OpenSearch',
        ),
    ]


@pytest.fixture
def expected_avhrr19_links() -> List[CswOnLineResource]:
    """Csw online resources for OSI-204 dataset fixture.

    Returns:
        List of online resources.
    """
    return [
        CswOnLineResource(
            protocol='WWW:OPENSEARCH',
            linkage='https://cmr.earthdata.nasa.gov/opensearch',
            name='C1655117103-PODAAC',
            description='NASA',
        ),
    ]


def test_parse_response():
    """Test Response parsing."""
    the_response = Response()
    the_response.status_code = 400
    the_response._content = b'bad xml'

    with pytest.raises(CswException):
        parse_response(the_response)

    the_response.status_code = 200
    with pytest.raises(CswException):
        parse_response(the_response)

    the_response._content = b'<test>abc</test>'
    xml = parse_response(the_response)
    assert isinstance(xml, lxml.etree._Element)


def test_get_number_of_results(csw_parser):
    """Test the retrieval of the number of results.

    Args:
        csw_parser: csw parser fixture
    """
    assert csw_parser.nb_metadata() == 3


def test_get_identifiers(csw_parser):
    """Test the retrieval of the identifiers.

    Args:
        csw_parser: csw parser fixture
    """
    assert csw_parser.identifiers() == [
        '2be4f201-df3d-40bc-8e9e-cd5ea8a3ed2a',
        '58eb8aa3-c3fb-4cc5-8236-c9a8ffaf7da1',
    ]


def test_get_dataset_links(csw_parser, expected_osi204_links):
    """Test the retrieval of opensearch online resources.

    Args:
        csw_parser: csw parser fixture
        expected_osi204_links: expected online resources
    """
    assert csw_parser.opensearch_links('2be4f201-df3d-40bc-8e9e-cd5ea8a3ed2a') is None

    links = csw_parser.opensearch_links('b140eac4-ebfa-4d06-8285-942e6aaedb77')
    assert links == CswDatasetLinks(
        dataset_id='DOI:10.15770/EUM_SAF_OSI_NRT_2013',
        opensearch_links=expected_osi204_links,
    )


def test_get_datasets(csw_parser, expected_avhrr19_links):
    """Test the retrieval of datasets and opensearch online resources.

    Args:
        csw_parser: csw parser fixture
        expected_avhrr19_links: expected online resources
    """
    assert csw_parser.datasets_opensearch_links() == {
        'AVHRR19_L-NAVO-L2P-v1.0': expected_avhrr19_links,
    }
