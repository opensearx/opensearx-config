"""Test HTTP Client."""
from typing import Dict

import pytest
from pydantic import ValidationError

from opensearx_config.http.client.base import HTTPClient
from opensearx_config.http.client.factory import create_http_client


@pytest.mark.parametrize('module_name, client_name, config', [
    ('opensearx_config.http.client', 'Base', {}),
    ('opensearx_config.http.client', 'Cas', {
        'cas_host': 'auth.ifremer.fr',
        'username': 'login',
        'password': 'password',
        'service_url': 'https://sextant.ifremer.fr/geonetwork/GHRSST/eng/catalog.search',
    }),
])
def test_http_client_factory_ok(module_name: str, client_name: str, config: Dict) -> None:
    """Test Base HTTP Client.

    Args:
        module_name: python module name
        client_name: client class name
        config: http client configuration
    """
    client = create_http_client(module_name=module_name, client_name=client_name, **config)
    assert isinstance(client, HTTPClient)


@pytest.mark.parametrize('module_name, client_name, config, exception', [
    ('opensearx_config.http.client', 'Fake', {}, ModuleNotFoundError),
    ('opensearx_config.http.client', 'Cas', {'cas_host': 'auth.ifremer.fr'}, ValidationError),
])
def test_http_client_factory_ko(module_name: str, client_name: str, config: Dict, exception) -> None:
    """Test Base HTTP Client.

    Args:
        module_name: python module name
        client_name: client class name
        config: http client configuration
        exception: expected exception
    """
    with pytest.raises(exception):
        create_http_client(module_name=module_name, client_name=client_name, **config)
