"""Test console scripts."""
from click.testing import CliRunner

from opensearx_config.cli import generate_configuration_cli
from opensearx_config.core.model import OpensearxConfig


def test_generate_configuration_cli(app_config_file, output_configuration_path, expected_configuration) -> None:
    """Test application `generate_configuration_cli`.

    Args:
        app_config_file: path to application configuration file
        output_configuration_path: output file path
        expected_configuration: expected generated configuration
    """
    runner = CliRunner()
    cli_result = runner.invoke(generate_configuration_cli, [
        '-c',
        app_config_file,
        '--output',
        output_configuration_path,
    ])
    assert cli_result.exit_code == 0

    generated_config = OpensearxConfig.from_yaml_file(output_configuration_path)
    assert generated_config == expected_configuration
