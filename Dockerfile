# syntax=docker/dockerfile:experimental

# ===========================================================
# RUNTIME BASE VENV
# ===========================================================
FROM condaforge/mambaforge:4.12.0-0 as conda-base

ARG CONDA_ENV_PATH="/venv/"
WORKDIR /tmp

COPY assets/conda/conda-dev-linux-64.lock ./
RUN --mount=type=cache,target=/opt/conda/pkgs \
    mamba create --copy -y -p $CONDA_ENV_PATH --file conda-dev-linux-64.lock && \
    conda run -p $CONDA_ENV_PATH python -m poetry config virtualenvs.create false --local

COPY poetry.lock pyproject.toml ./
RUN conda run -p $CONDA_ENV_PATH python -m poetry install --no-dev --no-root
#RUN conda run -p $CONDA_ENV_PATH python -m poetry install --no-dev

# ===========================================================
# DEV BASE VENV
# ===========================================================
FROM conda-base as base-development

# fix ModuleNotFoundError: No module named 'tomlkit'
# install poetry-dynamic-versioning (need a .git folder)
# install dev dependencies
RUN conda run -p $CONDA_ENV_PATH python -m pip install tomlkit==0.10.1 virtualenv && \
    git init && \
    conda run -p $CONDA_ENV_PATH python -m poetry add poetry-dynamic-versioning==0.14.0 && \
    conda run -p $CONDA_ENV_PATH python -m poetry install --no-root

FROM debian:buster-slim as development

ARG CONDA_ENV_PATH="/venv"
COPY --from=base-development $CONDA_ENV_PATH $CONDA_ENV_PATH
ENV PATH=$CONDA_ENV_PATH/bin:$PATH

# ============================================================
# RUNTIME FINAL VENV
# ============================================================
FROM conda-base as base-runtime

COPY . .
RUN conda run -p $CONDA_ENV_PATH python -m pip install --no-deps .

RUN find $CONDA_ENV_PATH -name '*.a' -delete && \
    rm -rf $CONDA_ENV_PATH/conda-meta && \
    rm -rf $CONDA_ENV_PATH/include && \
    find $CONDA_ENV_PATH -name '__pycache__' -type d -exec rm -rf '{}' '+' && \
    rm -rf $CONDA_ENV_PATH/lib/python*/site-packages/pip  \
        $CONDA_ENV_PATH/lib/python*/idlelib  \
        $CONDA_ENV_PATH/lib/python*/ensurepip \
        $CONDA_ENV_PATH/bin/x86_64-conda-linux-gnu-ld \
        $CONDA_ENV_PATH/bin/openssl \
        $CONDA_ENV_PATH/share/terminfo && \
    find $CONDA_ENV_PATH/lib/python*/site-packages -maxdepth 1  -mindepth 1  -name 'tests' -type d -exec rm -rf '{}' '+' && \
    find $CONDA_ENV_PATH/lib/python*/site-packages -name '*.pyx' -delete

FROM debian:buster-slim as runtime

ARG CONDA_ENV_PATH="/venv"
COPY --from=base-runtime $CONDA_ENV_PATH $CONDA_ENV_PATH
ENV PATH=$CONDA_ENV_PATH/bin:$PATH \
    PYTHONWARNINGS="ignore"
